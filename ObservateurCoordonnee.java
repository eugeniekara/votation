
public class ObservateurCoordonnee implements Observateur {
    private String modification = "PAS DE MODIFICATION";
    private String nom;
    
    @Override
    public void metsAJour(String attributModifie,
    Object valeur) {
        modification = "Attribut : " + attributModifie
        + " Valeur : " + valeur;
    }

    public String getModification() {
        return modification;
    }

    public ObservateurCoordonnee()
    {
        this.nom = "observbasique";
    }
    
    public ObservateurCoordonnee(String nom)
    {
      this.nom = nom;
    }
    
    public String getNom()
    {
        return nom;
    }
}
